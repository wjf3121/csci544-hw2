#!/usr/bin/env python3
import sys; sys.path.insert(0, "..")
from percepclassify import PerceptronClassifier
from utility.f_score import calc_f_score_all
import argparse
from feature_process import feature_with_context_and_word_shape
#from nbclassify import NBClassifier


def predict(words, classifier, processor):
	result = []
	tags = []
	pre, ppre = ('None', 'None'), ('None', 'None')
	for i, w in enumerate(words):
		next = 'None' if i == (len(words) - 1) else words[i + 1]
		nnext = 'None' if i > (len(words) - 3) else words[i + 2]
		f = processor(pre, ppre, w, next, nnext)
		p = classifier.predict(f)
		result.append('/'.join((w, p)))
		tags.append(p)
		pre, ppre = (w, p), pre
	return ' '.join(result), tags


def generate_dev_to_test(dev_path, output_path):
	with open(dev_path, 'r', encoding='utf-8', errors='ignore') as input_file:
		with open(output_path, 'w', encoding='utf-8', errors='ignore') as output_file:
			for line in input_file:
				words = ['/'.join(item.split('/')[:-1]) for item in line.split() if len(item.split('/')) >  1]
				output_file.write(' '.join(words) + '\n')

def get_correct(dev_path):
	tags = []
	with open(dev_path, 'r', encoding='utf-8', errors='ignore') as input_file:
		for line in input_file:
			t = [item.split('/')[-1] for item in line.split() if len(item.split('/')) > 1]
			tags.extend(t)
		return tags


def main():
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('MODEL',
						type=str,
						help='Name of the file that will contain the model that your classifier will learn')
	args = parser.parse_args()
	classifier = PerceptronClassifier(args.MODEL)
	tags = []
	for line in sys.stdin:
		if not line.strip(): break
		words = line.strip().split()
		result, tag = predict(words, classifier, feature_with_context_and_word_shape)
		tags.extend(tag)
		print(result)
	#correct = get_correct('./pos.dev')
	#calc_f_score_all(tags, correct)

if __name__ == '__main__':
	main()
