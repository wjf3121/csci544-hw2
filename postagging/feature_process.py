#!/usr/bin/env python3
import sys; sys.path.insert(0, '..')
from utility.word_shape import word_shape, word_shape_re

def plain_feature(pre, ppre, w, next, nnext):
	return [w]


def feature_with_context(pre, ppre, w, next, nnext):
	return [pre[1] + '_ft', ppre[1] + '_st', w, next + '_fw']

def feature_with_context_and_word_shape(pre, ppre, w, next, nnext):
	pre_w, ppre_w = pre[0], ppre[0]
	pre_t, ppre_t = pre[1], ppre[1]
	f = [w.lower(), 'ws_' + word_shape(w), '3suf_' + w[-3:]]
	f.extend([pre_t + '_ft', ppre_t + '_st',  next.lower() + '_fw'])
	f.append('wsf_' + word_shape(next))
	f.append(pre_w.lower() + '_' + pre_t + '_pwt')
	f.append(w.lower() + '_' + pre_w.lower() + '_wpfw')
	f.append(w.lower() + '_' + next.lower() + '_wfw')
	f.append(pre_w.lower() + '_' + ppre_w.lower() + '_pwfs')
	f.append(word_shape_re(w) + 'wsr')
	#f.append('3nsuf_' + next[-3:])
	#f.append('3npref_' + next[3:])
	#f.append(ppre_t + '_' + pre_t + '_ptfs')
	#f.append(w.lower() + '_' + pre_w.lower() + '_' + ppre_w.lower() + '_wpfsw')
	#f.append(w.lower() + '_' + next.lower() + '_' + nnext.lower() + '_wfsw')
	#f.append(pre_w.lower() + '_' + w.lower() + '_' + next.lower() + '_pwwfw')
	#f.append('2suf_' + w[-2:])
	#f.append('3pref_' + w[:3])
	#f.append('4suf_' + w[-4:])
	return f

