#!/usr/bin/env python3
import json
import argparse
import sys

class PerceptronClassifier:

	def __init__(self, weight_path):
		with open(weight_path, 'r') as weight_file:
			self.weights = json.load(weight_file)

	def __score(self, weight, feature):
		return sum([weight[f] if f in weight else 0 for f in feature])

	def predict(self, feature):
		if self.weights:
			feature.append('bias$')
			return max([(self.__score(self.weights[l], feature), l) for l in self.weights])[1]
		else:
			return None

def main():
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('MODELFILE',
						type=str,
						help='Name of the file that will contain the model that your classifier will learn')
	args = parser.parse_args()
	classifier = PerceptronClassifier(args.MODELFILE)
	for line in sys.stdin:
		if not line.strip(): break
		feature = line.strip().split()
		result = classifier.predict(feature)
		print(result)


if __name__ == '__main__':
	main()