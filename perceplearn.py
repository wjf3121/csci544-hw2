#!/usr/bin/env python3
import argparse
import json
import copy
import timeit
import sys

class PerceptronLearner():

    def __init__(self, training_path = None, dev_path=None, trains = None, devs = None):
        self.itr, self.count = 0, 0
        self.max_itr = 20 if dev_path or devs else 18
        self.min_itr = 12
        self.weights, self.acc = {}, {}
        if trains != None:
            self.__read_data(trains, devs)
        elif training_path != None:
            self.__read_data_from_file(training_path, dev_path)
        for l in set(self.labels):
            self.weights[l], self.acc[l] = {}, {}


    def __read_data(self, trains, devs):
        self.labels, self.features, self.dev_labels, self.devs = [], [], [], []
        for t in trains:
            self.__read_feature(t, self.features, self.labels)
        if devs:
            for d in devs:
                self.__read_feature(d, self.devs, self.dev_labels)

    def __read_data_from_file(self, training_path, dev_path):
        trains = self.__read_lines_from_file(training_path)
        devs = None if not dev_path else self.__read_lines_from_file(dev_path)
        self.__read_data(trains, devs)

    def __read_lines_from_file(self, file_path):
        with open(file_path, 'r', encoding='utf-8', errors='ignore') as input_file:
            return [line.split() for line in input_file]

    def __read_feature(self, data, feature_list, label_list):
        if len(data) > 1:
            label_list.append(data[0])
            f = data[1:]
            f.append('bias$')
            feature_list.append(f)


    def __score(self, weight, feature):
        return sum([weight[f] if f in weight else 0 for f in feature])

    def __avg_score(self, l, feature):
        if self.count > 0:
            return sum([(self.weights[l][f] if f in self.weights[l] else 0) - 
                (self.acc[l][f] if f in self.acc[l] else 0)/self.count for f in feature])
        return sum([(self.weights[l][f] if f in self.weights[l] else 0) for f in feature])

    def __predict(self, feature):
        scores = [(self.__score(self.weights[l], feature), l)
                  for l in self.weights]
        return max(scores)[1] if scores else None

    def __avg_predict(self, feature):
        scores = [(self.__avg_score(l, feature), l) for l in self.weights]
        return max(scores)[1] if scores else None

    def __calc_dev_accuracy(self):
        count = 0
        for f, l in zip(self.devs, self.dev_labels):
            if self.__avg_predict(f) == l:
                count += 1
        return count / len(self.devs)

    def __modify_weight(self, weight, feature, modified_val):
        for f in feature:
            weight[f] = modified_val + (weight[f] if f in weight else 0)

    
    def learn(self):
        for i, (f, l) in enumerate(zip(self.features, self.labels)):
            p = self.__predict(f)
            if p != l:
                self.__modify_weight(self.weights[l], f, 1)
                self.__modify_weight(self.weights[p], f, -1)
                self.__modify_weight(self.acc[l], f, 1 * self.count)
                self.__modify_weight(self.acc[p], f, -1 * self.count)
            self.count += 1
        self.itr += 1


    def iterative_learn(self):
        start = timeit.default_timer()
        prev, best = 0, 0
        best_weights, best_acc, best_count = None, None, None
        while self.itr < self.max_itr:
            self.learn()
            cur = self.__calc_dev_accuracy() if self.devs else self.itr
            print(cur, file = sys.stderr)
            if  cur < prev and self.itr > self.min_itr: 
                break
            if cur > best:
                best = cur
                best_weights = copy.deepcopy(self.weights)
                best_acc = copy.deepcopy(self.acc)
                best_count = self.count
            prev = cur
        if best != 0:
            self.weights = best_weights
            self.acc = best_acc
            self.count = best_count
        stop = timeit.default_timer()
        print(stop - start, file=sys.stderr )


    def write_model(self, file_path):
        output_dict = {}
        for l in self.weights:
            output_dict[l] = {}
            for f in self.weights[l]:
                val = self.weights[l][f] - (0 if self.count == 0 else (self.acc[l][f]/self.count))
                if val != 0:
                    output_dict[l][f] = val
        with open(file_path, 'w') as output_file:
            json.dump(output_dict, output_file)

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('TRAININGFILE',
                        type=str,
                        help='Name of the training file')
    parser.add_argument('MODELFILE',
                        type=str,
                        help='Name of the file that will contain the model that your classifier will learn')
    parser.add_argument('-h',
                        type=str,
                        help='Name of the dev file')
    args = parser.parse_args()
    p = PerceptronLearner(args.TRAININGFILE, args.h)
    p.iterative_learn()
    p.write_model(args.MODELFILE)

if __name__ == '__main__':
    main()
