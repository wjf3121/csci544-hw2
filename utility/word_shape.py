#!/usr/bin/env python3
import re

def word_shape_re(s):
	s = re.sub('[a-z]', 'a', s)
	s = re.sub('[A-Z]', 'A', s)
	s = re.sub('[0-9]', '9', s)
	s = re.sub('[^0-9a-zA-Z\']', '-', s)
	return s

def word_shape_re_seq(s):
	s = re.sub('[a-z]+', 'a', s)
	s = re.sub('[A-Z]+', 'A', s)
	s = re.sub('[0-9]+', '9', s)
	s = re.sub('[^0-9a-zA-Z\']+', '-', s)
	return s

def word_shape(s):
	length = len(s)
	if length == 0:
		return 'SYMBOL'
	cardinal = False
	number = True
	seenDigit = False
	seenNonDigit = False

	for i, ch in enumerate(s):
		digit = ch.isdigit()
		if digit:
			seenDigit = True
		else:
			seenNonDigit = True

		digit = digit or ch == '.' or ch == ',' or (i == 0 and (ch == '-' or ch == '+'))
		if not digit:
			number = False

	if not seenDigit:
		number = False
	elif not seenNonDigit:
		cardinal = True

	if cardinal: 
		if length < 4:
			return 'CARDINAL13'
		elif length == 4:
			return 'CARDINAL4'
		else:
			return 'CARDINAL5PLUS'
	elif number:
		return 'NUMBER'

	seenLower = False
	seenUpper = False
	allCaps = True
	allLower = True
	initCap = False
	dash = False
	period = False

	for i, ch in enumerate(s):
		up = ch.isupper()
		let = ch.isalpha()
		if ch == '-': 
			dash = True
		elif ch == '.':
			period = True
		if up:
			seenUpper = True
			allLower = False
		elif let:
			seenLower = True
			allCaps = False
		if i == 0 and up:
			initCap = True


	if length == 2 and initCap and period:
		return 'ACRONYM1'
	elif (seenUpper and allCaps and not seenDigit and period):
	  return 'ACRONYM'
	elif (seenDigit and dash and not seenUpper and not seenLower):
	  return 'DIGIT-DASH'
	elif (initCap and seenLower and seenDigit and dash):
	  return 'CAPITALIZED-DIGIT-DASH'
	elif (initCap and seenLower and seenDigit):
	  return 'CAPITALIZED-DIGIT'
	elif (initCap and seenLower and dash):
	  return 'CAPITALIZED-DASH'
	elif (initCap and seenLower):
	  return 'CAPITALIZED'
	elif (seenUpper and allCaps and seenDigit and dash):
	  return 'ALLCAPS-DIGIT-DASH'
	elif (seenUpper and allCaps and seenDigit):
	  return 'ALLCAPS-DIGIT'
	elif (seenUpper and allCaps and dash):
	  return 'ALLCAPS'
	elif (seenUpper and allCaps):
	  return 'ALLCAPS'
	elif (seenLower and allLower and seenDigit and dash):
	  return 'LOWERCASE-DIGIT-DASH'
	elif (seenLower and allLower and seenDigit):
	  return 'LOWERCASE-DIGIT'
	elif (seenLower and allLower and dash):
	  return 'LOWERCASE-DASH'
	elif (seenLower and allLower):
	  return 'LOWERCASE'
	elif (seenLower and seenDigit):
	  return 'MIXEDCASE-DIGIT'
	elif (seenLower):
	  return 'MIXEDCASE'
	elif (seenDigit):
	  return 'SYMBOL-DIGIT'
	else:
	  return "SYMBOL"
	

