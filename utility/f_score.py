#!/usr/bin/env python3
import sys 

def calc_accuracy(predicts, corrects):
	count = 0
	for r, t in zip(predicts, corrects):
		if r == t:
			count += 1
	return count / len(corrects)

def calc_precision_recall(predicts, corrects, label):
    retrieved_labels, relevant_labels, correct_labels = 0, 0, 0
    for predict, correct in zip(predicts, corrects):
        if predict == label:
            retrieved_labels += 1
        if correct == label:
            relevant_labels += 1
        if correct == label and predict == label:
            correct_labels += 1
    return (correct_labels / retrieved_labels) if retrieved_labels != 0 else 0, (correct_labels / relevant_labels) if relevant_labels != 0 else 0

def calc_f_score(predicts, corrects, label):
	precision, recall = calc_precision_recall(predicts, corrects, label)
	return precision, recall, (2 * precision * recall / (precision + recall)) if (precision + recall) != 0 else 0


def calc_f_score_all(predicts, corrects):
	labels = set(corrects)
	for label in sorted(labels):
		print(label + ' : ' + str(calc_f_score(predicts, corrects, label)), file = sys.stderr)
	print('overall : ' + str(calc_accuracy(predicts, corrects)), file = sys.stderr)

def read_result(file_path):
	result = []
	with open(file_path, 'r', encoding='utf-8', errors='ignore') as input_file:
		for line in input_file:
			result.append(line.strip())
	return result


def calc_precision_recall_entity(predicts, corrects, label = None):
	retrieved_labels, relevant_labels, correct_labels = 0, 0, 0
	for predict in predicts:
		if predict[0] == label or label == None:
			retrieved_labels += 1
			if predict in corrects:
				correct_labels += 1
	for correct in corrects:
		if correct[0] == label or label == None:
			relevant_labels += 1
	return (correct_labels / retrieved_labels) if retrieved_labels != 0 else 0, (correct_labels / relevant_labels) if relevant_labels != 0 else 0


def calc_f_score_entity(predicts, corrects, label = None):
	precision, recall = calc_precision_recall_entity(predicts, corrects, label)
	return precision, recall, (2 * precision * recall / (precision + recall)) if (precision + recall) != 0 else 0


def calc_f_score_all_entity(predicts, corrects):
	labels = set([correct[0] for correct in corrects])
	for label in sorted(labels):
		print(label + ' : ' + str(calc_f_score_entity(predicts, corrects, label)), file = sys.stderr)
	print('overall : ' + str(calc_f_score_entity(predicts, corrects)), file = sys.stderr)



'''
correct_pos = read_result('./correct.pos')
correct_ner = read_result('./correct.ner')
result_pos = read_result('./result_pos')
result_ner = read_result('./result_ner')
calc_f_score_all(result_pos,correct_pos)
print()
calc_f_score_all(result_ner,correct_ner)
'''