#!/usr/bin/env python3
from collections import defaultdict
import argparse
import json

TFIDF = '_tfidf'

def generate_model(training_path):
    all_words, label_words_dict, label_count = set(), defaultdict(lambda : defaultdict(int)), defaultdict(int)
    word_doc_dict = defaultdict(lambda : defaultdict(int))
    tfidf = True
    with open(training_path, 'r') as traininig_file:
        for line in traininig_file:
            feature = line.split()
            if feature:
                label = feature[0]
                label_count[label] += 1
                feature_set = set(feature[1:])
                for word in feature[1:]:
                    if not word.endswith(TFIDF): tfidf = False
                    all_words.add(word)
                    label_words_dict[label][word] += 1
                for word in feature_set:
                    word_doc_dict[label][word] += 1
    return all_words, label_words_dict, label_count, word_doc_dict, tfidf


def write_model_to_file(model_path, model):
    all_words, label_words_dict, label_count, word_doc_dict, tfidf = model
    model = {'vocabulary': len(
        all_words), 'label_words_dict': label_words_dict, 'label_count': label_count, 'tfidf' : tfidf}
    if tfidf:
        model['word_doc_dict'] = word_doc_dict
    with open(model_path, 'w') as model_file:
        json.dump(model, model_file)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('TRAININGFILE',
                        type=str,
                        help='Name of the training file')
    parser.add_argument('MODELFILE',
                        type=str,
                        help='Name of the file that will contain the model that your classifier will learn')
    args = parser.parse_args()

    write_model_to_file(args.MODELFILE, generate_model(args.TRAININGFILE))

if __name__ == '__main__':
    main()
