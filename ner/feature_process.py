#!/usr/bin/env python3
import sys; sys.path.insert(0, '..')
from utility.word_shape import word_shape, word_shape_re, word_shape_re_seq

def plain_feature(pre, ppre, t, next, nnext):
	return [t[0], t[1] + '_t']

def feature_with_context(pre, ppre, t, next, nnext):
	pre_w, ppre_w = pre[2], ppre[2]
	pre_ne, ppre_ne = pre[0], ppre[0]
	pre_tag, ppre_tag = pre[1], ppre[1]
	next_w, nnext_w = next[0], nnext[0]
	next_t,nnext_t = next[1], nnext[1]

	f = [ppre_w.lower() + '_ppw', pre_w + '_pw',  pre_ne + '_fn', ppre_ne + '_sn', 
			pre_tag + '_ft', ppre_tag + '_st', t[0], t[1] + '_t', next_w + '_nfw', 
			next_t+ '_nft', nnext_w + '_nsw', nnext_t + '_nst']
	return f

def feature_with_context_and_word_shape(pre, ppre, t, next, nnext):
	pre_w, ppre_w = pre[2], ppre[2]
	pre_ne, ppre_ne = pre[0], ppre[0]
	pre_tag, ppre_tag = pre[1], ppre[1]
	next_w, nnext_w = next[0], nnext[0]
	next_t,nnext_t = next[1], nnext[1]

	f = [ppre_w.lower() + '_ppw', pre_w.lower() + '_pw',  pre_ne + '_fn', ppre_ne + '_sn', 
			pre_tag + '_ft', ppre_tag + '_st', t[0].lower() + '_c', t[1] + '_t', next_w.lower() + '_nfw', 
			next_t+ '_nft', nnext_w.lower() + '_nsw', nnext_t + '_nst']
	#f.extend([(i + '_ws') for i in word_shape(t[0]).split('-')])
	f.append('ws_' + word_shape(t[0]))
	f.append('wsf_' + word_shape(next_w))
	f.append('wsp_' + word_shape(pre_w))
	f.append('wsr_' + word_shape_re(t[0]))
	f.append('3suf_' + t[0][-3:])
	f.append('3pref_' + t[0][:3])
	f.append(ppre_w + '_' + pre_w + '_pwfs')
	f.append(t[0][:4]+'_4pref')
	#f.append(t[0][:-4]+'_ptfs')
	#f.append(word_shape(t[0])+'_'+next_t+'_wlen')
	#f.append(word_shape(t[0])+'_'+pre_ne  +'_ntefs')
	#f.append(word_shape(t[0])+'_'+next_w+'_ntfs')
	#f.append(t[1] + '_' + pre_tag + '_' + ppre_tag + '_xxxxx')
	#f.append('3prefnsuf_' + t[0][:3] + '_' + next_w[-3:])
	#f.append('3prefnnsuf_' + t[0][:3] + '_' + nnext_w[-3:])
	# f.append('3psuf_' + pre_w[-3:])
	# f.append('3ppref_' + pre_w[:3])
	# f.append('3nsuf_' + next_w[-3:])
	# f.append('3npref_' + next_w[:3])
	#f.append(next_w.lower() + nnext_w.lower() + '_wfsw')
	#f.append(pre_tag + '_' + t[0].lower() + '_' + next_t + 'nnnfs')
	#f.append(t[0].lower() + pre_w.lower() + ppre_w.lower() + '_wpfsw')
	#f.append(t[0].lower() + '_' + pre_w.lower() + '_wpfw')
	#f.append(t[0].lower() + next_w.lower() + '_wfw')
	#f.append(t[0].lower() + next_w.lower() + nnext_w.lower() + '_wfsw')
	return f

