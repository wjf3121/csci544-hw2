#!/usr/bin/env python3
import sys; sys.path.insert(0, '..')
from perceplearn import PerceptronLearner
import argparse
from feature_process import *

def generate_features_and_labels(file_path, processor, dev = False):
	words_tags = []
	with open(file_path, 'r', encoding='latin-1', errors='ignore') as input_file:
		for line in input_file:
			f = [list(reversed(item.split('/'))) for item in line.split() if len(item.split('/')) > 2]
			words_tags.extend(process_features(f, processor, dev))
		return words_tags


def process_features(features, processor, dev = False):
	new_features = []
	pre, ppre = ('None', 'None', 'None'), ('None', 'None', 'None')
	for i, f in enumerate(features):
		next_w = 'None' if i == (len(features) - 1) else features[i + 1][2]
		next_t = 'None' if i == (len(features) - 1) else features[i + 1][1]
		nnext_w = 'None' if i > (len(features) - 3) else features[i + 2][2]
		nnext_t = 'None' if i > (len(features) - 3) else features[i + 2][1]
		new_feature = processor(pre, ppre, ('/'.join(f[2:][::-1]), f[1]), (next_w, next_t), (nnext_w, nnext_t))
		new_feature.insert(0, f[0])
		new_feature.extend([f[2] for f in features[max(i - 3, 0):min( i + 4, len(features))] if len(f) == 3])
		new_features.append(new_feature)
		pre, ppre = (f[0] , f[1], '/'.join(f[2:][::-1])), pre
	return new_features


def learn(training_file, dev_file):
	p = PerceptronLearner(training_file, dev_file)
	p.iterative_learn()
	return p

def write_feature(file_path, features):
	with open(file_path, 'w') as output_file:
		for feature in features:
			output_file.write(' '.join(feature) + '\n')


def main():
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('TRAININGFILE',
						type=str,
						help='Name of the training file')
	parser.add_argument('MODEL',
						type=str,
						help='Name of the file that will contain the model that your classifier will learn')
	parser.add_argument('-h',
						type=str,
						help='Name of the dev file')
	args = parser.parse_args()
	trains = generate_features_and_labels(args.TRAININGFILE, feature_with_context_and_word_shape)
	devs = None
	if args.h:
		devs = generate_features_and_labels(args.h, feature_with_context_and_word_shape, True)
	p = PerceptronLearner(trains = trains, devs = devs)
	p.iterative_learn()
	p.write_model(args.MODEL)

if __name__ == '__main__':
	main()

