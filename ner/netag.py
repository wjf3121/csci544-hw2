#!/usr/bin/env python3
import sys; sys.path.insert(0, "..")
from percepclassify import PerceptronClassifier
from utility.f_score import calc_f_score_all_entity
import argparse
import codecs
from feature_process import *
#from nbclassify import NBClassifier

def predict(tuples, classifier, processor):
	tags = []
	pre, ppre = ('None', 'None', 'None'), ('None', 'None', 'None')
	result = []
	for i, t in enumerate(tuples):
		next = ('None', 'None') if i == (len(tuples) - 1) else ('/'.join(tuples[i + 1][:-1]), tuples[i + 1][-1])
		nnext = ('None', 'None') if i > (len(tuples) - 3) else ('/'.join(tuples[i + 2][:-1]), tuples[i + 2][-1])
		f = processor(pre, ppre, ('/'.join(t[:-1]), t[-1]), next, nnext)
		f.extend([f[0] for f in tuples[max(i - 3, 0):min( i + 4, len(tuples))] if len(f) == 2])
		p = classifier.predict(f)
		result.append('/'.join(('/'.join(t), p)))
		tags.append(p)
		pre, ppre = (p, t[-1], '/'.join(t[:-1])), pre
	return ' '.join(result), tags

 
def generate_dev_to_test(dev_path, output_path):
	with open(dev_path, 'r', encoding='latin-1', errors='ignore') as input_file:
		with open(output_path, 'w', encoding='latin-1', errors='ignore') as output_file:
			for line in input_file:
				words = []
				for seq in line.split():
					items = seq.split('/')
					if len(items) > 2:
						words.append('/'.join(items[:-1]))
				output_file.write(' '.join(words) + '\n')

def get_correct(dev_path):
	tags = []
	with open(dev_path, 'r', encoding='latin-1', errors='ignore') as input_file:
		for i, line in enumerate(input_file):
			t = [item.split('/')[-1] for item in line.split() if len(item.split('/')) > 2]
			tags.extend(get_entities(t, i))
		return tags


def get_entities(tags, line = 0):
	prev = None
	cur = None
	entities = []
	for i, t in enumerate(tags):
		e = t.split('-')
		if len(e) > 1 and e[0] == 'I' and e[1] == prev:
			continue
		if cur != None: 
			cur.append(i - 1)
			entities.append(tuple(cur))
		if len(e) > 1 and e[0] == 'B':
			cur = [e[1], line, i]
			prev = e[1]
		else:
			prev, cur = None, None
	if cur != None:
		cur.append(len(tags) - 1)
		entities.append(tuple(cur))
	return entities

def main():
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('MODEL',
						type=str,
						help='Name of the file that will contain the model that your classifier will learn')
	args = parser.parse_args()
	classifier = PerceptronClassifier(args.MODEL)
	tags = []
	sys.stdin = codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
	for i, line in enumerate(sys.stdin):
		if not line.strip(): break
		tuples = [item.split('/') for item in line.strip().split()]
		result, tag = predict(tuples, classifier, feature_with_context_and_word_shape)
		tags.extend(get_entities(tag, i))
		print(result)
	#correct = set(get_correct('./ner.esp.dev'))
	#calc_f_score_all_entity(tags, correct)

if __name__ == '__main__':
	main()
